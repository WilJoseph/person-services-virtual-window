package org.example.security;

import org.example.model.Role;
import org.example.model.User;
import org.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AuthProvider implements AuthenticationProvider {
    // To create your own custom authentication using DB, first create a class that implements AuthenticationProvider
    // Then override the methods authenticate() and supports()
    // Remember to inject the repository dependency needed to query the DB

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // You can get the credentials entered into Spring Security by using the Authentication object
        // You must return UsernamePasswordAuthenticationToken, which is derived from Authentication object.
        String email = authentication.getName();
        String pwd = authentication.getCredentials().toString();
        User person = userRepository.findByEmail(email);
        if(null != person && person.getUserId() > 0 &&
                passwordEncoder.matches(pwd, person.getPassword())){
            return new UsernamePasswordAuthenticationToken(
                    email, pwd, getGrantedAuthorities(person.getRoles()));
            // because person.getName is used in the UsernamePasswordAuthenticationToken, then you will be able to use the actual name of the user rather than the username.
            // You can also use the email so that you can query it from the DB and just use the DB mapped data to display the name.
            // This also replaces the authentication object.
        }else{
            throw new BadCredentialsException("Invalid credentials!");
        }
    }

    // This grants the ROLE for Spring Security integration
    private List<GrantedAuthority> getGrantedAuthorities(Role roles) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+roles.getRoleName()));
        return grantedAuthorities;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
