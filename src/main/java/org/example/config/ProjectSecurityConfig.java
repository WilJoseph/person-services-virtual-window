package org.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class ProjectSecurityConfig {

    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf().ignoringRequestMatchers("/user/create")
                .and()
                .authorizeHttpRequests()
                .requestMatchers("/user/callHR").authenticated()
                .requestMatchers("/user/join").authenticated()
                .requestMatchers("/user/memberForm").authenticated()
                .requestMatchers("/user/create").permitAll()
                .requestMatchers("/home").authenticated()
                .requestMatchers("/startCall").authenticated()
                .requestMatchers("/endCall").authenticated()
                .and()
                .formLogin(Customizer.withDefaults());

        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
