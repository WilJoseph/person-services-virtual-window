package org.example.repository;

import org.example.model.Call;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CallRepository extends JpaRepository<Call, Integer> {
    List<Call> findAllByOrderByCallIdDesc();
    List<Call> findAllByStatus(String status);
    List<Call> findAllByStatusOrderByCallId(String status);
    List<Call> findAllByStatusNotOrderByCallIdDesc(String status);
}
