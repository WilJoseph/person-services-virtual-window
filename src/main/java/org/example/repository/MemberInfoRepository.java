package org.example.repository;

import org.example.model.MemberInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberInfoRepository extends JpaRepository<MemberInfo, Integer> {
    MemberInfo findByCallCallId(int callId);
}
