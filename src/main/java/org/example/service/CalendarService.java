package org.example.service;

import com.google.api.services.calendar.model.*;
import org.example.model.Call;
import org.example.model.MemberInfo;
import org.example.model.User;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public interface
CalendarService {
    public Event setEventSummary(String summary, String location, String description);

    public ConferenceData setConferenceData();

    public EventDateTime setStart(Calendar startDate);
    public EventDateTime setEnd(Calendar endDate);

    public String[] setEventRecurrence();

    public EventAttendee[] setEventAttendees();

    public EventReminder[] setEventReminderOverrides();

    public Event createMeetEvent(Event event, EventDateTime start, EventDateTime end);

    public Event insertMeetEvent(Event event, com.google.api.services.calendar.Calendar service);

    public List<Event> checkEvents(com.google.api.services.calendar.Calendar service) throws IOException;


    public EventDateTime getLatestEventStart(Event lastEvent);

    public EventDateTime getLatestEventEnd(Event lastEvent);

    public Call createNewEvent(Event newEvent);

    public List<Call> getCallEvents();

    public User setUserCallInformation(Call call);

    public Call updateCallToInProgress(int callId);

    public Call updateCallToComplete(int callId);

    public MemberInfo setMemberInfoToCall(MemberInfo member, Call call);

    public List<Call> findCallByStatus(String status);

    public List<Call> findCallByStatusAsc(String status);

    public List<Call> findCallByStatusNot(String status);

    public List<Call> findAllQueueItems();

    public Call findCallById(int callId);

    public MemberInfo findMemberInfoByCall(int callId);
}
