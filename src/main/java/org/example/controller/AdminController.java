package org.example.controller;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.*;
import org.example.model.Call;
import org.example.model.GoogleToken;
import org.example.model.MemberInfo;
import org.example.model.User;
import org.example.repository.UserRepository;
import org.example.service.CalendarService;
import org.example.startup.GoogleCalendarStartup;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
//import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    CalendarService calendarService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GoogleCalendarStartup googleCalendarStartup;

    @Autowired
    OAuth2AuthorizedClientService clientService;

    @GetMapping({"", "/home", "/"})
    public String showIndex(Model model, @RequestParam(value= "verified",required = false) String verified){
        List<Call> queueItems = calendarService.findCallByStatus("IN QUEUE");
        List<Call> userQueue = new ArrayList<>();
        List<Call> progressItems = calendarService.findCallByStatus("In Progress");
        //TODO: Migrate to service
        User userEntity = userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        userEntity.getCalls();
        Call accepted = null;
        Call memberInQueue = null;
        int queueNumber = 0;
        String name = userEntity.getLastName();
        String role = userEntity.getRoles().toString();
        boolean isMemberInQueue = false;

        List<Call> progressQueue = new ArrayList<>();
        for (Call call : userEntity.getCalls()){
            if (call.getStatus().equalsIgnoreCase("Accepted") || call.getStatus().equalsIgnoreCase("In Progress")){
                accepted = call;
            }
            if (call.getStatus().equalsIgnoreCase("IN QUEUE")){
                userQueue.add(call);
            }
        }

        if(userEntity.getRoles().getRoleName().equalsIgnoreCase("MEMBER")) {
            for (Call call : userEntity.getCalls()) {
                if (call.getStatus().equalsIgnoreCase("IN QUEUE")) {
                    memberInQueue = call;
                    isMemberInQueue = true;
                }
            }

            if(isMemberInQueue) {
                for (int i = 0; i < queueItems.size(); i++) {
                    if (queueItems.get(i).getCallId() == memberInQueue.getCallId()) {
                        queueNumber = i;
                    }
                }
            }
        }

        // Member will not be able to see all numbers in queue
//        if(userEntity.getRoles().getRoleName().equalsIgnoreCase("MEMBER")){
//            queueItems = new ArrayList<>();
//            for (Call call : userEntity.getCalls()){
//                if (!call.getStatus().equalsIgnoreCase("Completed")){
//                    queueItems.add(call);
//                }
//            }
//        }


        String verifiedCall = null;
        verifiedCall = verified;

        model.addAttribute("name", name);
        model.addAttribute("role", role);
        model.addAttribute("inProgressQueue", progressItems);
        model.addAttribute("isMemberInQueue", isMemberInQueue);
        model.addAttribute("userQueue", userQueue);
        model.addAttribute("memberQueueNumber", queueNumber);
        model.addAttribute("member", new MemberInfo());
        model.addAttribute("verified", verifiedCall);
        model.addAttribute("accepted", accepted);
        model.addAttribute("queue", queueItems);
        return "index.html";
    }

    @GetMapping("/startCall")
    public String startCall(@RequestParam int callId, Model model){
        Call callEntity = calendarService.updateCallToInProgress(callId);
        model.addAttribute("call", callEntity);
        return "redirect:/home";
    }

    @GetMapping("/endCall")
    public String endCall(@RequestParam int callId){
        Call callEntity = calendarService.updateCallToComplete(callId);
        return "redirect:/home";
    }


//    @RequestMapping("/events")
//    public String getEvents(Model model) throws IOException, GeneralSecurityException {
//        //////// Create an authentication thingy here
//
////        Authentication authentication =
////                SecurityContextHolder.getContext().getAuthentication();
////        String accessToken = null;
////        if (authentication.getClass()
////                .isAssignableFrom(OAuth2AuthenticationToken.class)) {
////            OAuth2AuthenticationToken oauthToken =
////                    (OAuth2AuthenticationToken) authentication;
////            String clientRegistrationId =
////                    oauthToken.getAuthorizedClientRegistrationId();
////            if (clientRegistrationId.equals("google")) {
////                OAuth2AuthorizedClient client = clientService.loadAuthorizedClient(
////                        clientRegistrationId, oauthToken.getName());
////                accessToken = client.getAccessToken().getTokenValue();
////            }
////        }
////
////        // Create the GoogleCredentials
////        GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
////        Calendar newService = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
////                .setApplicationName("People Services Virtual Window")
////                .build();
//
//        /////////
//
//        Calendar service = googleCalendarStartup.getCalendarService();
//
//        DateTime now = new DateTime(System.currentTimeMillis());
//
//        LocalDate today = LocalDate.now();
//        ZoneId calendarTimeZone = ZoneId.of(service.calendarList().get("primary").execute().getTimeZone());
//        LocalDateTime startOfDay = LocalDateTime.of(today, LocalTime.MIDNIGHT);
//        LocalDateTime endOfDay = LocalDateTime.of(today, LocalTime.MAX);
//
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
//        DateTime startDateTime = new DateTime(startOfDay.atZone(calendarTimeZone).withZoneSameInstant(ZoneId.of("UTC")).format(formatter));
//        DateTime endDateTime = new DateTime(endOfDay.atZone(calendarTimeZone).withZoneSameInstant(ZoneId.of("UTC")).format(formatter));
//
//        Events events = service.events().list("primary")
//                .setMaxResults(10)
//                .setTimeMin(startDateTime)
//                .setTimeMax(endDateTime)
//                .setOrderBy("startTime")
//                .setSingleEvents(true)
//                .execute();
//        List<Event> items = events.getItems();
//        if (items.isEmpty()) {
//            System.out.println("No upcoming events found.");
//        } else {
//            System.out.println("Upcoming events");
//            for (Event event : items) {
//                DateTime start = event.getStart().getDateTime();
//                if (start == null) {
//                    start = event.getStart().getDate();
//                }
//                System.out.printf("%s (%s)\n", event.getSummary(), start);
//            }
//        }
//
//        model.addAttribute("events", items);
//        return "events.html";
//    }

    public AdminController() throws GeneralSecurityException, IOException {
    }
}
