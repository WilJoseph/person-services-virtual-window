package org.example.controller;

import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.ConferenceSolution;
import com.google.api.services.calendar.model.EntryPoint;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import org.example.model.Call;
import org.example.model.MemberInfo;
import org.example.model.User;
import org.example.repository.CallRepository;
import org.example.service.CalendarService;
import org.example.startup.GoogleCalendarStartup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    CalendarService calendarService;

    @Autowired
    GoogleCalendarStartup googleCalendarStartup;
    @Autowired
    private CallRepository callRepository;

    @RequestMapping("/callHR")
    public String callHR(Model model, @ModelAttribute(name="member") MemberInfo member) throws IOException, GeneralSecurityException {

        Calendar service = googleCalendarStartup.getCalendarService();

        // Aside maybe from the summary details of an event, all the others could be inside one service method.
        Event event = calendarService.setEventSummary("Test Event", "Virtual", "Test Description");

        java.util.Calendar startDate = java.util.Calendar.getInstance();
        java.util.Calendar endDate =  (java.util.Calendar) startDate.clone();
        EventDateTime start;
        EventDateTime end;

        // Check if there are existing events
        List<Event> calendarEvents = calendarService.checkEvents(service);
        if(null != calendarEvents && calendarEvents.size() > 0){
            Event lastEvent = calendarEvents.get(calendarEvents.size()-1);
            start = calendarService.getLatestEventStart(lastEvent);
            end = calendarService.getLatestEventEnd(lastEvent);
        } else {
            start = calendarService.setStart(startDate);
            endDate.add(java.util.Calendar.MINUTE, 30);
            end = calendarService.setEnd(endDate);
        }

        calendarService.createMeetEvent(event, start, end);
        event = calendarService.insertMeetEvent(event, service);

        // Once meet event created, log it in DB
        // Create the event in the database
        Call callEntity = calendarService.createNewEvent(event);
        // Map the event to the user who created the call
        User userEntity = calendarService.setUserCallInformation(callEntity);
        // Map the member info to the call
        MemberInfo memberInfo = calendarService.setMemberInfoToCall(member, callEntity);
        System.out.printf("Event created: %s\n", event.getHtmlLink());
        model.addAttribute("meetLink", "https://meet.google.com/" + event.getConferenceData().getConferenceId());
        model.addAttribute("eventLink", event.getHtmlLink());
        return "redirect:/home";
    }

    @PostMapping("/join")
    public String joinCall(Model model, @RequestParam("xID") String xID, @RequestParam("callId") int callId){
        // Find xID in Call ID
        String returnUri = "redirect:/home";
        MemberInfo memberInfo = calendarService.findMemberInfoByCall(callId);
        if (memberInfo.getXID().equalsIgnoreCase(xID)){
            calendarService.updateCallToInProgress(callId);
            returnUri = "redirect:/home?verified=true";
        }

        return returnUri;
    }

    @RequestMapping("/memberForm")
    public String goToMemberInformation(Model model){
        model.addAttribute("member", new MemberInfo());
        return "memberinfo";
    }
}
