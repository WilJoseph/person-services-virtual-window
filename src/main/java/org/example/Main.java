package org.example;

import org.example.startup.GoogleCalendarStartup;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.IOException;
import java.security.GeneralSecurityException;

@SpringBootApplication
@EnableJpaRepositories("org.example.repository")
@EntityScan("org.example.model")
@ComponentScan
@EnableJpaAuditing
public class Main {

    public static void main(String[] args) throws IOException, GeneralSecurityException {
        SpringApplication.run(Main.class, args);
    }

}