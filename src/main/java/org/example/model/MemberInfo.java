package org.example.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class MemberInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int memberId;

    private String email;
    private String givenName;
    private String lastName;
    private String xID;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST, targetEntity = Call.class)
    @JoinColumn(name = "call_id", referencedColumnName = "callId",nullable = false)
    private Call call;
}
