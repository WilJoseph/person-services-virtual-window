package org.example.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
public class User extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;
    private String email;
    private String password;
    private String givenName;
    private String lastName;
    private String xID;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST, targetEntity = Role.class)
    @JoinColumn(name = "role_id", referencedColumnName = "roleId",nullable = false)
    private Role roles;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_calls", // table to use
            joinColumns = {
                    @JoinColumn(name = "user_id", referencedColumnName = "userId")}, // Primary Key of this entity
            inverseJoinColumns = {
                    @JoinColumn(name = "call_id", referencedColumnName = "callId")})
    private Set<Call> calls = new HashSet<>();
}
