package org.example.model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name="call_session")
public class Call extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int callId;

    private String status;

    private String meetLink;

    private LocalDateTime endDate;


    // Mapped by calls from the userEntity
    @ManyToMany(mappedBy = "calls",fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Set<User> attendees = new HashSet<>();
}
