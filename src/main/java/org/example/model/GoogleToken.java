package org.example.model;

import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.util.ArrayList;
import java.util.List;

public class GoogleToken extends ApiBinding{

    private static final String GET_CALENDAR_LIST="https://www.googleapis.com/calendar/v3/calendars/primary/events";

    private static final String GET_CALENDAR_EVENT="https://www.googleapis.com/calendar/v3/calendars/primary/events/eventId";

    public GoogleToken (String accessToken) {
        super(accessToken);
    }

    public Events getEvents(){

        Events response = restTemplate.getForObject(GET_CALENDAR_LIST, Events.class);
        return response;
    }

}
